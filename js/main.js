let data = [
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  },
  {
    server: 'S402',
    name: 'Việt Hưng',
    mana: '2364878',
    class: '600',
    vip: '11'
  }
]

function section (id) {
  const section = document.getElementById(id);
  const windowHeight = window.innerHeight;
  const elementTop = section.getBoundingClientRect().top;
  const elementVisible = 50;

  if (elementTop < windowHeight - elementVisible) {
    section.classList.add('active');
  } else {
    section.classList.remove('active');
  }
}

function goToSection(id) {
  const section = document.getElementById(id);
  section.scrollIntoView({
    behavior: 'smooth'
  });
}

$(document).ready(function() {
  window.addEventListener("scroll", () => section('section-2'));
  window.addEventListener("scroll", () => section('section-3'));

  for (let i = 1; i < data.length; i++) {
    $("#table").append("<tr>" +
      "                <td>" + i + 3 + "</td>" +
      "                <td>" + data[i].server + "</td>" +
      "                <td>" + data[i].name + "</td>" +
      "                <td>" + data[i].mana + "</td>" +
      "                <td>" + data[i].class + "</td>" +
      "                <td>" + data[i].vip +"</td>" +
      "              </tr>");
  }

  $(".slideshow").slick({
    infinite: true,
    autoplay: false,
    arrows: true,
    autoplaySpeed: 4000,
    nextArrow:"<img class='nextButton' src='/images/button/next.png' alt='next button'>",
    prevArrow:"<img class='preButton' src='/images/button/next.png' alt='next button'>",
  });

  $("#left-neo").addClass( "active" );
  $("#right-neo").addClass( "active" );

  $( "#play-button" ).click(function() {
    document.body.classList.add('overlay')
    $("#modal").show(500)
  });

  $( "#layout-hide" ).click(function() {
    document.body.classList.remove('overlay')
    $("#modal").removeClass('overlay').hide(500)
  });

  $( "#cancel-hide" ).click(function() {
    document.body.classList.remove('overlay')
    $("#modal").removeClass('overlay').hide(500)
  });

  // đổi button
  const bxhImageWrapper = document.getElementById('bxhImageWrapper')
  const noidungduatopImageWrapper = document.getElementById('noidungduatopImageWrapper')

  $( "#duatop" ).click(function() {
    $("#bxh").attr("src", "/images/button/bxh-fade.png");
    $("#duatop").attr("src", "/images/button/duatop-no-fade.png");

    bxhImageWrapper.classList.add('hide')
    bxhImageWrapper.classList.remove('show')

    noidungduatopImageWrapper.classList.add('show')
    noidungduatopImageWrapper.classList.remove('hide')
  });

  $( "#bxh" ).click(function() {
    $("#bxh").attr("src", "/images/button/bxh-no-fade.png");
    $("#duatop").attr("src", "/images/button/duatop-fade.png");

    bxhImageWrapper.classList.add('show')
    bxhImageWrapper.classList.remove('hide')

    noidungduatopImageWrapper.classList.add('hide')
    noidungduatopImageWrapper.classList.remove('show')
  });

  // di chuyển tới section
  $( "#toSection2" ).click(function() {
    goToSection('section-2')
  });

  $( "#toSection3" ).click(function() {
    goToSection('section-3')
  });

  $( "#toStartPage" ).click(function() {
    goToSection('section-1')
  });
});


